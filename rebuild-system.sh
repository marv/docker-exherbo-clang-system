#!/usr/bin/env bash
# vim: set sw=4 sts=4 ts=4 et tw=80 :

if [[ ${#} -lt 1 ]]; then
    echo "ERROR: You need to specify the host, no argument given"
    exit 1
fi

HOST=${1}

set -e

docker build \
    --no-cache \
    --build-arg HOST=${HOST} \
    -f Dockerfile.rebuild-system \
    -t marvins/exherbo-${HOST}-clang-rebuilt-system \
    .

docker push marvins/exherbo-${HOST}-clang-rebuilt-system

