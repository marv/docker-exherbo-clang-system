#!/usr/bin/env bash
# vim: set sw=4 sts=4 ts=4 et tw=80 :

if [[ ${#} -lt 1 ]]; then
    echo "ERROR: You need to specify the cross host, no argument given"
    exit 1
fi

HOST=${1}

if [[ ${HOST} == x86_64-*-*-gnu ]]; then
    PROFILE=amd64/clang
elif [[ ${HOST} == x86_64-*-*-musl ]]; then
    PROFILE=amd64/musl/clang
else
    echo "ERROR: Could not determine profile for host"
    exit 2
fi

set -e

docker build \
    --no-cache \
    --build-arg HOST=${HOST} \
    --build-arg PROFILE=${PROFILE} \
    -f Dockerfile.bootstrap \
    -t marvins/exherbo-${HOST}-clang-toolchain-bootstrapped \
    .

docker push marvins/exherbo-${HOST}-clang-toolchain-bootstrapped

